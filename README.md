# @ethroncat/verdaccio

[![NPM version](https://img.shields.io/npm/v/@ethroncat/verdaccio.svg)](https://npmjs.org/package/@ethroncat/verdaccio)
[![Total downloads](https://img.shields.io/npm/dt/@ethroncat/verdaccio.svg)](https://npmjs.org/package/@ethroncat/verdaccio)

[Ethron.js](http://ethronlabs.com) catalog for [Verdaccio](https://verdaccio.org).

## Install

```
npm i -g --production @ethroncat/verdaccio
```

## Tasks

```
ethronjs -c @ethroncat/verdaccio
```

### Notes

When `run` task executed, the `config.yaml` to use is `./config.yaml`.
If this not defined, a default file used.
We can generate it with the generator [@ethrongen/verdaccio](https://www.npmjs.com/package/@ethrongen/verdaccio).
