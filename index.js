//imports
const {cat} = require("ethron");
const docker = require("@ethronpi/docker");
const exec = require("@ethronpi/exec");
const fs = require("@ethronpi/fs");
const verdaccio = require("@ethronpi/verdaccio");
const xdg = require("@ethronpi/xdg");
const path = require("path");
const fsx = require("fs-extra");

//Verdaccio container home.
const home = path.join(process.env.HOME, "opt/verdaccio");

//Image to use.
const image = "verdaccio/verdaccio";

//Container name.
const container = "verdaccio";

//Use sudo?
const sudo = true;

//Time to wait.
const sleep = "2s";

//Config file to use.
const config = (
  fsx.existsSync(path.join(process.cwd(), "config.yaml")) ?
    path.join(process.cwd(), "config.yaml") :
    path.join(__dirname, "tmpl/config.yaml")
);

//catalog
cat.call("console", xdg.open, {
  target: "http://localhost:4873"
}).title("Open Web console");

cat.call("pull", docker.pull, {
  image
}).title("Pull Verdaccio image");

cat.macro("run", [
  [exec, `sudo rm -rf ${home}`],
  [fs.mkdir, home],
  [fs.cp, config, path.join(home, "conf", "config.yaml")],
  [fs.mkdir, path.join(home, "storage")],
  [fs.mkdir, path.join(home, "plugins")],
  [exec, `sudo chown -R 100:101 ${home}`],
  [verdaccio.run, {home, image, container, publish: 4873, sudo, sleep}],
  cat.get("console")
]).title("Run Verdaccio container");

cat.macro("start", [
  [verdaccio.start, {container, sudo, sleep}],
  cat.get("console")
]).title("Start Verdaccio container");

cat.call("stop", verdaccio.stop, {
  container,
  sudo
}).title("Stop Verdaccio container");

cat.call("rm", docker.rm, {
  container,
}).title("Remove Verdaccio container");

cat.macro("dflt", [
  cat.get("start")
]).title("Start Verdaccio container");
