//imports
const {cat} = require("ethron");
const eslint = require("@ethronpi/eslint");
const npm = require("@ethronpi/npm");

//Package name.
const pkg = require("./package.json").name;

//Who can publish the package on NPM.
const who = "ethroncat";

//catalog
cat.call("dflt", eslint, {
  src: "."
}).title("Lint source code");

cat.call("pub", npm.publish, {
  who,
  access: "public",
  path: "."
}).title("Publish on NPM");

cat.call("install", npm.install, {
  pkg: "."
}).title("Install the catalog globally");
